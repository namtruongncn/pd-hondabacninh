<?php
/**
 * Copyright template part.
 *
 * @package  hondabacninh
 */

if ( ! pd_option( 'copyright', null, false ) ) {
	return;
}
?>

<div class="copyright text-center">
	<div class="container">
		<?php pd_option( 'copyright' ); ?>
	</div>
</div>
