<?php
/**
 * Footer template part.
 *
 * @package  hondabacninh
 */

if ( ! is_active_sidebar( 'fixed-area' ) ) {
	return;
}

?>

<div class="fixed-area clearfix">
	<?php if ( is_active_sidebar( 'fixed-area' ) ) : ?>
		<?php dynamic_sidebar( 'fixed-area' ); ?>
	<?php endif; ?>
</div><!-- .fixed-area -->
